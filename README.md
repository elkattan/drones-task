# Drone API

Drone API is a RESTFul API that records and tracks drones state, weight and payload throughout it's medication delivery journey.

## Disclaimer
This project was made using some technologies that I found interesting and learned using while making the project, some of the practices I used may not be ideal - and I would appreciate other's comments - but this is not a full representation of my skills since it was constrained by time and knowledge of the used technologies, despite that, I will continue improving the project while learning more about the cool language Golang and it's libraries. 

## Assumptions
+ Authentication/Authorization is out of this project score
+ Drone can carry different medications not multiples of the same medication (it's addition won't make much of a difference)
+ Only Validation and Function testing needed
+ No specific commit format is required
+ Audit log only require human readability


## Notes
+ The project is implemented in an MVC structure
+ Database used is a portable SQLite in-memory database
+ Demo data can ve loaded by setting `DATABASE_NAME` environment variable

## Testing the project
Only routes package implement testes and the testes in the routes package can be run as follows
```sh
go test ./routes
```
Also for reviewers a Postman collection is present at the project root directory with most project endpoints

## Run the project
Project can be run by changing directory to the project root folder
```sh
cd $GOPATH/github.com/elkattan/drones-task
```
Then running main.go file
```sh
go run main.go
```
This will start the HTTP server with an empty in-memory database, you can set the `DATABASE_NAME` environment variable to any name to use file on disk as a database or use the preloaded "demo.db" database name for a pre-loaded database before running the server.

You can also use the containerized project through building the container
```sh
docker build -t drone-task-elkattan .
```
Then running the container
```sh
docker run -p 8080:8080 -it --rm drone-task-elkattan
```

## Building the project
Building the project is even easier
```sh
go build main.go
```
This will produce an executable that can be run with any server (Very portable)
## Known issues
This project is just an illustration of basic skills and is not meant to go live, therefore here is a list of known issues that was not handled due to lack of time mostly and it's small impact on the overall project review.

1. Loading medications into drone, medications are not validated to be existing, mainly due to the inefficiency in the obvious method of validation and finding a more efficient method require time to dive deeper in Gorm capabilities, in the mean time, the only issue in the current approach is the ugly, out-of-context error message.
2. Error handling, ideally an API should return predefined error codes or messages that the client side can parse/understand easily, this had no much impact on this project score therefore ditched.
3. Better logging, backend must have it's own logging, but i found it was enough to implement the basic state log in database for this project size.
4. Project is NOT production ready, the project was build with the mentality of illustrating skills and not building real world application, therefore there will be no environment configuration, auto generated docs or CI/CD
