package models

import (
	"time"

	"gorm.io/gorm"
)

type Drone struct {
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        gorm.DeletedAt `gorm:"index"`
	Serial           string         `gorm:"primarykey;size:100" json:"serial" create:"min=10,max=100,nonzero,nonnil" update:"max=100,nonnil"`
	DroneModel       uint           `json:"model" create:"max=4,nonzero,nonnil" update:"max=4,nonnil"`                                // 1:Lightweight, 2:Middleweight, 3:Cruiserweight, 4:Heavyweight
	State            uint           `json:"state" create:"max=6,nonzero,nonnil" update:"max=6,nonnil"`                                // 1:IDLE, 2:LOADING, 3:LOADED, 4:DELIVERING, 5:DELIVERED, 6:RETURNING
	Weight           uint           `gorm:"check:weight < 501" json:"weight" create:"max=500,nonzero,nonnil" update:"max=500,nonnil"` // Max Weight in grams
	Battery          uint           `json:"battery" create:"max=100,nonzero,nonnil" update:"max=100,nonnil"`                          // Current Battery percentage
	CurrentPayloadID uint           `json:"current_payload_id"`
	CurrentPayload   Payload        `json:"current_payload"`
}

type Medication struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
	Name      string         `json:"name" create:"regexp=^[a-zA-Z0-9-_]*$,nonzero,nonnil" update:"regexp=^[a-zA-Z0-9-_]*$,nonnil"`
	Weight    uint           `json:"weight" create:"max=500,nonzero,nonnil" update:"max=500,nonnil"`
	Code      string         `gorm:"primarykey" json:"code" create:"regexp=^[A-Z0-9_]*$,nonzero,nonnil" update:"regexp=^[A-Z0-9_]*$,nonnil"`
	Image     string         `json:"image" create:"nonnil" update:"nonnil"`
	Payloads  []Payload      `gorm:"many2many:payload_medications;" json:"payloads"`
}

type Payload struct {
	gorm.Model
	DroneSerial string       `json:"drone_serial"`
	Medications []Medication `gorm:"many2many:payload_medications;" json:"medications"`
}

type Log struct {
	gorm.Model
	Event   string `json:"event"`
	Message string `json:"message"`
}
