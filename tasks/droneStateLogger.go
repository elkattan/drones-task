package tasks

import (
	"fmt"
	"time"

	"github.com/elkattan/drones-task/models"
	"gorm.io/gorm"
)

func RunDroneStateLogger(db *gorm.DB, duration time.Duration) {
	for true {
		states := map[uint]string{
			1: "IDLE",
			2: "LOADING",
			3: "LOADED",
			4: "DELIVERING",
			5: "DELIVERED",
			6: "RETURNING",
		}

		droneModels := map[uint]string{
			1: "Lightweight",
			2: "Middleweight",
			3: "Cruiserweight",
			4: "Heavyweight",
		}
		var drones []models.Drone
		db.Model(&models.Drone{}).Find(&drones)

		for _, drone := range drones {
			var payload models.Payload
			if drone.CurrentPayloadID != 0 {
				db.Model(&models.Payload{}).Where("drone_serial = ?", drone.Serial).First(&payload)
			}
			weight := 0
			for _, medication := range payload.Medications {
				weight += int(medication.Weight)
			}
			log := models.Log{
				Event:   "DRONE_STATE_LOG",
				Message: fmt.Sprintf("Drone %s of model %s is %s with battery level %d%% and payload with %d medications that weights %d/%d grams", drone.Serial, droneModels[drone.DroneModel], states[drone.State], drone.Battery, len(payload.Medications), weight, drone.Weight),
			}
			db.Model(&models.Log{}).Create(&log)
		}

		time.Sleep(duration)
	}
}
