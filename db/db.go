package db

import (
	"os"

	"github.com/elkattan/drones-task/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func SetupDatabase() *gorm.DB {
	dbName := os.Getenv("DATABASE_NAME")
	if dbName == "" {
		dbName = "file::memory:?cache=shared"
	}

	db, err := gorm.Open(sqlite.Open(dbName), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate models
	db.AutoMigrate(&models.Drone{}, &models.Medication{}, &models.Payload{}, &models.Log{})

	return db
}
