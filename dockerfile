FROM golang:1.18
RUN mkdir /app 
ADD . /app/
WORKDIR /app
ENV DATABASE_NAME="demo.db"
ENV GIN_MODE=release
RUN go build -o drone_task .
CMD ["./drone_task"]