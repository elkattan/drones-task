package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRoutes(r *gin.Engine, db *gorm.DB) {
	SetupDronesRoutes(r, db)
	SetupMedicationsRoutes(r, db)
}
