package routes

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/elkattan/drones-task/db"
	"github.com/elkattan/drones-task/models"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var dronesServer *gin.Engine
var validDrone models.Drone = models.Drone{
	Serial:     "1111111111",
	State:      1,
	DroneModel: 1,
	Weight:     430,
	Battery:    90,
}
var invalidDrone models.Drone = models.Drone{
	Serial:     "111111",
	State:      0,
	DroneModel: 1,
	Weight:     600,
	Battery:    101,
}

var testDrone = models.Drone{
	Serial:     "2222222222",
	State:      1,
	DroneModel: 3,
	Weight:     430,
	Battery:    24,
}
var testMedication = models.Medication{
	Code:   "MEDIC_3",
	Name:   "Medication-1",
	Weight: 100,
}

func TestDroneCreation(t *testing.T) {
	dronesServer = gin.Default()
	DB := db.SetupDatabase()
	SetupDronesRoutes(dronesServer, DB)

	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(validDrone)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/drone/", &buf)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 201, w.Code)
}

func TestDroneValidation(t *testing.T) {
	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(invalidDrone)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/drone/", &buf)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 400, w.Code)
}

func TestDroneListRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/drone/", nil)
	dronesServer.ServeHTTP(w, req)

	var drones []models.Drone
	json.NewDecoder(w.Body).Decode(&drones)

	assert.Equal(t, 200, w.Code)
	assert.GreaterOrEqual(t, len(drones), 1)
}

func TestDroneGetByIdRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/drone/"+validDrone.Serial+"/", nil)

	dronesServer.ServeHTTP(w, req)

	var drone models.Drone
	json.NewDecoder(w.Body).Decode(&drone)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, validDrone.Serial, drone.Serial)
	assert.Equal(t, validDrone.DroneModel, drone.DroneModel)
	assert.Equal(t, validDrone.State, drone.State)
	assert.Equal(t, validDrone.Battery, drone.Battery)
}

func TestDroneUpdateRoute(t *testing.T) {
	var buf bytes.Buffer
	newDrone := models.Drone{
		State:      3,
		DroneModel: 2,
		Weight:     200,
		Battery:    26,
	}
	json.NewEncoder(&buf).Encode(newDrone)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "/api/drone/"+validDrone.Serial+"/", &buf)
	dronesServer.ServeHTTP(w, req)

	var drone models.Drone
	json.NewDecoder(w.Body).Decode(&drone)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, newDrone.DroneModel, drone.DroneModel)
	assert.Equal(t, newDrone.Weight, drone.Weight)
	assert.Equal(t, newDrone.Battery, drone.Battery)
}

func TestDroneDeleteRoute(t *testing.T) {
	// Making a delete request
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/drone/"+validDrone.Serial+"/", nil)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 204, w.Code)

	// Making a GET request to confirm deletion
	dw := httptest.NewRecorder()
	dReq, _ := http.NewRequest("GET", "/api/drone/"+validDrone.Serial+"/", nil)
	dronesServer.ServeHTTP(dw, dReq)

	assert.Equal(t, 404, dw.Code)
}

func TestDroneLoadingStateValidation(t *testing.T) {
	DB := db.SetupDatabase()

	// Inserting required data to test loading
	DB.Model(&testDrone).Create(&testDrone)
	DB.Model(&testMedication).Create(&testMedication)

	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode([]string{testMedication.Code})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/api/drone/"+testDrone.Serial+"/load/", &buf)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 400, w.Code)
	assert.Equal(t, "{\"message\":\"Drone is not in loading state\"}", w.Body.String())
}

func TestDroneLoadingBatteryValidation(t *testing.T) {
	// Attempting to update state to LOADING
	var stateBuf bytes.Buffer
	json.NewEncoder(&stateBuf).Encode(models.Drone{
		State: 2,
	})

	sw := httptest.NewRecorder()
	sReq, _ := http.NewRequest("PATCH", "/api/drone/"+testDrone.Serial+"/", &stateBuf)
	dronesServer.ServeHTTP(sw, sReq)

	// Fails due to low battery
	assert.Equal(t, 400, sw.Code)

	var batteryBuf bytes.Buffer
	json.NewEncoder(&batteryBuf).Encode(models.Drone{
		Battery: 25,
	})

	// Updating battery percentage to 25 or above
	bw := httptest.NewRecorder()
	bReq, _ := http.NewRequest("PATCH", "/api/drone/"+testDrone.Serial+"/", &batteryBuf)
	dronesServer.ServeHTTP(bw, bReq)

	// Update success
	assert.Equal(t, 200, bw.Code)

	json.NewEncoder(&stateBuf).Encode(models.Drone{
		State: 2,
	})

	// Attempting to change state to LOADING again
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "/api/drone/"+testDrone.Serial+"/", &stateBuf)
	dronesServer.ServeHTTP(w, req)

	println(w.Body.String())

	// Change success since battery level is acceptable
	assert.Equal(t, 200, w.Code)
}

func TestDroneLoadingRoute(t *testing.T) {
	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode([]string{testMedication.Code})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/api/drone/"+testDrone.Serial+"/load/", &buf)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestDroneLoadedDataRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/drone/"+testDrone.Serial+"/load/", nil)
	dronesServer.ServeHTTP(w, req)

	var payload models.Payload
	json.NewDecoder(w.Body).Decode(&payload)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, testDrone.Serial, payload.DroneSerial)
	assert.Equal(t, testMedication.Code, payload.Medications[0].Code)
}

func TestDroneUnloadingRoute(t *testing.T) {
	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode([]string{testMedication.Code})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/drone/"+testDrone.Serial+"/load/", &buf)
	dronesServer.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	gw := httptest.NewRecorder()
	gReq, _ := http.NewRequest("GET", "/api/drone/"+testDrone.Serial+"/load/", nil)
	dronesServer.ServeHTTP(gw, gReq)

	var payload models.Payload
	json.NewDecoder(gw.Body).Decode(&payload)

	assert.Equal(t, 200, gw.Code)
	assert.Equal(t, testDrone.Serial, payload.DroneSerial)
	assert.Equal(t, 0, len(payload.Medications))
}
