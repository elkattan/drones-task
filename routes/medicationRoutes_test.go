package routes

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/elkattan/drones-task/db"
	"github.com/elkattan/drones-task/models"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var medicServer *gin.Engine
var validMedication models.Medication = models.Medication{
	Code:   "MEDIC_1",
	Weight: 430,
	Name:   "Medication-1",
	Image:  "https://www.slagelse.info/wp-content/uploads/2020/03/medicin-Clker-Free-Vector-Images-medicine-296966_1280.png",
}
var invalidMedication models.Medication = models.Medication{
	Code:   "some-medicine",
	Weight: 530,
	Name:   "Medication 2",
}

func TestMedicationCreation(t *testing.T) {
	medicServer = gin.Default()
	DB := db.SetupDatabase()
	SetupMedicationsRoutes(medicServer, DB)

	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(validMedication)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/medication/", &buf)
	medicServer.ServeHTTP(w, req)

	assert.Equal(t, 201, w.Code)
}

func TestMedicationValidation(t *testing.T) {
	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(invalidMedication)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/medication/", &buf)
	medicServer.ServeHTTP(w, req)

	assert.Equal(t, 400, w.Code)
}

func TestMedicationListRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/medication/", nil)
	medicServer.ServeHTTP(w, req)

	var medications []models.Medication
	json.NewDecoder(w.Body).Decode(&medications)

	assert.Equal(t, 200, w.Code)
	assert.GreaterOrEqual(t, len(medications), 1)
}

func TestMedicationGetByIdRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/medication/"+validMedication.Code+"/", nil)

	medicServer.ServeHTTP(w, req)

	var medication models.Medication
	json.NewDecoder(w.Body).Decode(&medication)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, validMedication.Code, medication.Code)
	assert.Equal(t, validMedication.Name, medication.Name)
	assert.Equal(t, validMedication.Weight, medication.Weight)
	assert.Equal(t, validMedication.Image, medication.Image)
}

func TestMedicationUpdateRoute(t *testing.T) {
	var buf bytes.Buffer
	newMedication := models.Medication{
		Weight: 75,
		Name:   "Updated_Medication",
	}
	json.NewEncoder(&buf).Encode(newMedication)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PATCH", "/api/medication/"+validMedication.Code+"/", &buf)
	medicServer.ServeHTTP(w, req)

	var medication models.Medication
	json.NewDecoder(w.Body).Decode(&medication)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, newMedication.Name, medication.Name)
	assert.Equal(t, newMedication.Weight, medication.Weight)
}

func TestMedicationDeleteRoute(t *testing.T) {
	// Making a delete request
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/medication/"+validMedication.Code+"/", nil)
	medicServer.ServeHTTP(w, req)

	var medication models.Medication
	json.NewDecoder(w.Body).Decode(&medication)

	assert.Equal(t, 204, w.Code)

	// Making a GET request to confirm deletion
	dw := httptest.NewRecorder()
	dReq, _ := http.NewRequest("GET", "/api/medication/"+validMedication.Code+"/", nil)
	medicServer.ServeHTTP(dw, dReq)

	assert.Equal(t, 404, dw.Code)
}
