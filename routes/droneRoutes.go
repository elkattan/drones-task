package routes

import (
	"net/http"

	"github.com/elkattan/drones-task/controllers"
	"github.com/elkattan/drones-task/models"
	"github.com/elkattan/drones-task/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupDronesRoutes(r *gin.Engine, db *gorm.DB) {

	router := r.Group("/api/drone")
	controller := controllers.DroneController{DB: db}

	router.GET("/", func(ctx *gin.Context) {
		availableOnly := ctx.Query("availableOnly") == "true"
		var drones []models.Drone
		if err := controller.GetAllDrones(&drones, availableOnly); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, drones)
		}
	})

	router.GET("/:serial/", func(ctx *gin.Context) {
		var drone models.Drone
		serial := ctx.Param("serial")
		if err := controller.GetDroneBySerial(serial, &drone); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, drone)
		}
	})

	router.GET("/:serial/battery/", func(ctx *gin.Context) {
		var drone models.Drone
		serial := ctx.Param("serial")
		if err := controller.GetDroneBySerial(serial, &drone); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"battery": drone.Battery,
			})
		}
	})

	router.POST("/", func(ctx *gin.Context) {
		var drone models.Drone
		if err := utils.ValidateBody(ctx, &drone, func() error { return controller.CreateDrone(&drone) }); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusCreated, drone)
		}
	})

	router.PATCH("/:serial/", func(ctx *gin.Context) {
		var drone models.Drone
		serial := ctx.Param("serial")
		if err := utils.ValidateBody(ctx, &drone, nil); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			if err := controller.UpdateDrone(serial, &drone); err != nil {
				status := http.StatusBadRequest
				if err.Error() == "record not found" {
					status = http.StatusNotFound
				}
				ctx.JSON(status, gin.H{"message": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, drone)
			}
		}
	})

	router.DELETE("/:serial/", func(ctx *gin.Context) {
		serial := ctx.Param("serial")
		if err := controller.DeleteDrone(serial); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusNoContent, nil)
		}
	})

	router.GET("/:serial/load/", func(ctx *gin.Context) {
		var payload models.Payload
		serial := ctx.Param("serial")
		if err := controller.GetDroneLoad(serial, &payload); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": "Drone Not Found"})
		} else {
			ctx.JSON(http.StatusOK, payload)
		}
	})

	router.PUT("/:serial/load/", func(ctx *gin.Context) {
		var payloads []string
		serial := ctx.Param("serial")
		if err := ctx.ShouldBindJSON(&payloads); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			if err := controller.LoadDrone(serial, payloads); err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"message": "Data Loaded"})
			}
		}
	})

	router.DELETE("/:serial/load/", func(ctx *gin.Context) {
		var payloads []string
		serial := ctx.Param("serial")
		if err := ctx.ShouldBindJSON(&payloads); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			if err := controller.UnloadDrone(serial, payloads); err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"message": "Data Unloaded"})
			}
		}
	})
}
