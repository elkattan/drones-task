package routes

import (
	"net/http"

	"github.com/elkattan/drones-task/controllers"
	"github.com/elkattan/drones-task/models"
	"github.com/elkattan/drones-task/utils"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupMedicationsRoutes(r *gin.Engine, db *gorm.DB) {

	router := r.Group("/api/medication")
	controller := controllers.MedicationController{DB: db}

	router.GET("/", func(ctx *gin.Context) {
		var medications []models.Medication
		if err := controller.GetAllMedications(&medications); err != nil {
			ctx.JSON(http.StatusBadRequest, err.Error())
		} else {
			ctx.JSON(http.StatusOK, medications)
		}
	})

	router.GET("/:code/", func(ctx *gin.Context) {
		var medication models.Medication
		code := ctx.Param("code")
		if err := controller.GetMedicationByCode(code, &medication); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, medication)
		}
	})

	router.POST("/", func(ctx *gin.Context) {
		var medication models.Medication
		if err := utils.ValidateBody(ctx, &medication, func() error { return controller.CreateMedication(&medication) }); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusCreated, medication)
		}
	})

	router.PATCH("/:code/", func(ctx *gin.Context) {
		var medication models.Medication
		code := ctx.Param("code")
		if err := utils.ValidateBody(ctx, &medication, nil); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		} else {
			// handling the case where the medication is not found
			if err := controller.UpdateMedication(code, &medication); err != nil {
				ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, medication)
			}
		}
	})

	router.DELETE("/:code/", func(ctx *gin.Context) {
		code := ctx.Param("code")
		if err := controller.DeleteMedication(code); err != nil {
			ctx.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		} else {
			ctx.JSON(http.StatusNoContent, nil)
		}
	})
}
