package controllers

import (
	"github.com/elkattan/drones-task/models"
	"gorm.io/gorm"
)

type MedicationController struct {
	DB *gorm.DB
}

// GetMedicationByCode returns the medication of the code string provided
func (c *MedicationController) GetMedicationByCode(code string, dest *models.Medication) error {
	return c.DB.Model(&models.Medication{}).Where("code = ?", code).First(dest).Error
}

// GetAllMedications returns all medications available
func (c *MedicationController) GetAllMedications(dest *[]models.Medication) error {
	return c.DB.Model(&models.Medication{}).Find(dest).Error
}

// CreateMedication creates a new medication
func (c *MedicationController) CreateMedication(dest *models.Medication) error {
	return c.DB.Model(&models.Medication{}).Create(dest).Error
}

// UpdateMedication updates the medication with the provided data
func (c *MedicationController) UpdateMedication(medicationCode string, dest *models.Medication) error {
	return c.DB.Model(&models.Medication{}).Where("code = ?", medicationCode).UpdateColumns(dest).Error
}

// DeleteMedication deletes the medication of the code string provided
func (c *MedicationController) DeleteMedication(medicationCode string) error {
	return c.DB.Model(&models.Medication{}).Where("code = ?", medicationCode).First(&models.Medication{}).Delete(&models.Medication{}).Error
}
