package controllers

import (
	"errors"

	"github.com/elkattan/drones-task/models"
	"github.com/elkattan/drones-task/utils"
	"gorm.io/gorm"
)

type DroneController struct {
	DB *gorm.DB
}

// GetDroneBySerial returns the drone of the serial string provided
func (c *DroneController) GetDroneBySerial(serial string, dest *models.Drone) error {
	return c.DB.Model(&models.Drone{}).Where("serial = ?", serial).First(dest).Error
}

// GetAllDrones returns all drones in case `availableOnly` is false
// and returns IDLE state drones only in case `availableOnly` is true
func (c *DroneController) GetAllDrones(dest *[]models.Drone, availableOnly bool) error {
	if availableOnly {
		return c.DB.Model(&models.Drone{}).Where("state = ?", 1).Find(dest).Error
	}
	return c.DB.Model(&models.Drone{}).Find(dest).Error
}

// CreateDrone creates a new drone
func (c *DroneController) CreateDrone(dest *models.Drone) error {
	if dest.State == 2 && dest.Battery < 25 {
		return errors.New("Drone with battery level under 25% cannot be in loading state")
	}
	return c.DB.Model(&models.Drone{}).Create(dest).Error
}

// UpdateDrone updates the drone with the provided data
func (c *DroneController) UpdateDrone(droneSerial string, dest *models.Drone) error {
	var drone models.Drone
	if err := c.DB.Model(&models.Drone{}).Where("serial = ?", droneSerial).First(&drone).Error; err != nil {
		return err
	}
	// TODO: Better handling for dest.Battery being 0
	// In case of request update battery to 0
	// The API wont be able to validate if the value is a default or user defined value

	// Checking if state is loading and battery below 25%
	if dest.State == 2 && ((dest.Battery < 25 && dest.Battery > 0) || (dest.Battery == 0 && drone.Battery < 25)) {
		return errors.New("Drone with battery level under 25% cannot be in loading state")
	}
	// Point 3 in Functional requirements
	// Prevent the drone from "BEING IN" LOADING state if the battery level is below 25%
	// If the battery level ever dropped under 25% while in loading state
	// the state should be changed back to IDLE state
	if dest.Battery < 25 && dest.Battery > 0 && drone.State == 2 && dest.State == 0 {
		dest.State = 1
	}

	// On payload delivered deatech CurrentPayload from the drone
	if dest.State == 5 {
		dest.CurrentPayloadID = 0
	}

	return c.DB.Model(&models.Drone{}).Where("serial = ?", droneSerial).UpdateColumns(dest).Error
}

// DeleteDrone deletes the drone with the serial string provided
func (c *DroneController) DeleteDrone(droneSerial string) error {
	return c.DB.Model(&models.Drone{}).Where("serial = ?", droneSerial).First(&models.Drone{}).Delete(&models.Drone{}).Error
}

// GetDroneLoad returns the payload of the drone with the serial string provided
func (c *DroneController) GetDroneLoad(droneSerial string, payload *models.Payload) error {
	var drone models.Drone
	if err := c.DB.Model(&models.Drone{}).Where("serial = ?", droneSerial).First(&drone).Error; err != nil {
		return err
	}

	if drone.CurrentPayloadID == 0 {
		return errors.New("Drone has no payload")
	}

	return c.DB.Model(&models.Payload{}).Where("id = ?", drone.CurrentPayloadID).Preload("Medications").First(payload).Error
}

// LoadDrone add the medications of the `medicationCodes` provided to the
// current payload of the drone with the serial string provided
func (c *DroneController) LoadDrone(droneSerial string, medicationCodes []string) error {
	var drone models.Drone
	if err := c.DB.Model(&drone).Where("serial = ?", droneSerial).First(&drone).Error; err != nil {
		return err
	}

	if drone.State != 2 {
		return errors.New("Drone is not in loading state")
	}

	var medications []models.Medication
	for _, code := range medicationCodes {
		medications = append(medications, models.Medication{Code: code})
	}

	payload := models.Payload{
		DroneSerial: droneSerial,
		Medications: medications,
	}

	newWeight, err := utils.GetMedicationsTotalWeight(c.DB, medicationCodes)
	if err != nil {
		return err
	}

	if newWeight > int(drone.Weight) {
		return errors.New("Drone weight exceeded")
	}

	// Checking if a payload already attached
	if drone.CurrentPayloadID == 0 {
		// Create new payload with medications
		return c.DB.Transaction(func(tx *gorm.DB) error {
			if err := tx.Model(&payload).Create(&payload).Error; err != nil {
				return err
			} else {
				drone.CurrentPayloadID = payload.ID
				return tx.Model(&drone).UpdateColumn("CurrentPayloadID", payload.ID).Error
			}
		})
	} else {

		// Append medications to existing payload
		drone.CurrentPayload.ID = drone.CurrentPayloadID
		payloadWeight, _ := utils.GetPayloadTotalWeight(c.DB, int(drone.CurrentPayloadID))
		println(payloadWeight, newWeight, drone.Weight)
		if payloadWeight+newWeight > int(drone.Weight) {
			return errors.New("Drone weight exceeded")
		}
		return c.DB.Model(&drone.CurrentPayload).Association("Medications").Append(&medications)
	}
}

// UnloadDrone removes the medications of the `medicationCodes` provided from the
// current payload of the drone with the serial string provided
func (c *DroneController) UnloadDrone(droneSerial string, medicationCodes []string) error {
	var drone models.Drone
	if err := c.DB.Model(&drone).Where("serial = ?", droneSerial).First(&drone).Error; err != nil {
		return err
	}

	if drone.CurrentPayloadID == 0 {
		return errors.New("Drone has no payload")
	}

	drone.CurrentPayload.ID = drone.CurrentPayloadID

	var medications []models.Medication
	for _, code := range medicationCodes {
		medications = append(medications, models.Medication{Code: code})
	}

	return c.DB.Model(&drone.CurrentPayload).Association("Medications").Delete(&medications)
}
