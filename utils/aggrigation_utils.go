package utils

import (
	"github.com/elkattan/drones-task/models"
	"gorm.io/gorm"
)

func GetMedicationsTotalWeight(db *gorm.DB, medicationCodes []string) (int, error) {
	var result int
	err := db.Model(&models.Medication{}).Select("sum(weight)").Where("code IN ?", medicationCodes).Row().Scan(&result)
	return result, err
}

func GetPayloadTotalWeight(db *gorm.DB, payloadId int) (int, error) {
	var result int
	subQuery := db.Table("payload_medications").Where("payload_id = ?", payloadId).Select("medication_code")
	err := db.Model(&models.Medication{}).Select("sum(weight)").Where("code IN (?)", subQuery).Row().Scan(&result)
	return result, err
}
