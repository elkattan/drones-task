package utils

import (
	"github.com/gin-gonic/gin"
	"gopkg.in/validator.v2"
)

func ValidateBody(ctx *gin.Context, obj interface{}, callback func() error) error {
	if err := ctx.ShouldBindJSON(obj); err != nil {
		return err
	} else {
		// callback will provided in case of creation only
		if callback == nil {
			validator.SetTag("update")
		} else {
			validator.SetTag("create")
		}
		if errs := validator.Validate(obj); errs != nil {
			return errs
		} else {
			if callback != nil {
				if err := callback(); err != nil {
					return err
				}
			}
		}
	}
	return nil
}
