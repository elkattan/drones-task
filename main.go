package main

import (
	"time"

	"github.com/elkattan/drones-task/db"
	"github.com/elkattan/drones-task/routes"
	"github.com/elkattan/drones-task/tasks"
	"github.com/gin-gonic/gin"
)

func main() {
	// Initialize server and database
	r := gin.Default()
	db := db.SetupDatabase()

	// Running background tasks
	go tasks.RunDroneStateLogger(db, 30*time.Second)

	// Setting up routes
	routes.SetupRoutes(r, db)

	r.Run()
}
